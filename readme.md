

Raspberry Pi 1 (A, B, A+, B+, Zero, Zero W)

The systems now known as Raspberry Pi model 1 models A and B were announced in February 2012. This family was expanded by very similar models A+ and B+. In 2015, the Zero model was announced, using the same CPU as the 1 family but with a smaller form factor, followed by the Zero W, which adds wireless connectivity.

The first generation Raspberry systems work using Debian armel. The Raspberry Pi 1's processor falls uncomfortably between the processor families that Debian has chosen to target, between armel and armhf. While Raspberry Pi OS solves this to some degree an unofficial port will always give less certainty than an official one.

The Zero uses the same SoC/CPU as the first version, so they should function identically. 

The CPU in the Raspberry Pi 1 and Zero implements the ARMv6 ISA (with VFP2) and is thus incompatible with the Debian armhf port baseline of ARMv7+VFP3 and ARM hardware-floating-point ports for other distributions, which all have the same baseline. It is compatible with Debian armel (armv4t, soft(emulated) FP), but floating-point tasks will be slow when running the Debian armel port. (This doesn't stand for newer families) 


http://ftp.uk.debian.org/debian/pool/main/l/lighttpd/lighttpd_1.4.64-1_armel.deb

